const express = require('express')
const app = express()
const port = 3000

var { Exercises, Users } = require('./models/exercise-data')

app.get('/', (req, res)=> {
  res.status(200).send("Landing Page | <a href='/push'>Push Exercises</a> | <a href='/pull'>Pull Exercises</a>")
})

app.get('/pull', (req, res)=>{
  var pullExercises = Exercises.pull
  res.status(200).send(JSON.stringify(pullExercises))
})

app.get('/push', (req, res)=>{
  var pushExercises = Exercises.push
  res.status(200).send(JSON.stringify(pushExercises))
})

// Middleware for unhandled routes
app.use(function (req, res, next) {
// app.use(function (err, req, res, next) <-- for error handling middleware
  res.status(404).send("Sorry can't find that!")
  // console.log(err.stack) <-- with the error handling middleware
})

app.listen(port, ()=> console.log("Second log") )

module.exports = app;