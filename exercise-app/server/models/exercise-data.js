exercises = {
  push: [
    { name: "push ups", reps: 15, sets: 4, setRest: 60, endRest: 180 },
    { name: "shoulder press", reps: 12, sets: 3, setRest: 60, endRest: 180 }
  ],
  pull: [
    { name: "bicep curls", reps: 15, sets: 4, setRest: 60, endRest: 180 },
    { name: "barbell rows", reps: 15, sets: 4, setRest: 60, endRest: 180 }
  ],
  legs: [
    { name: "squats", reps: 15, sets: 4, setRest: 60, endRest: 180 },
    { name: "lunges", reps: 15, sets: 4, setRest: 60, endRest: 180 }
  ],
  abs: [
    { name: "sit ups", reps: 15, sets: 4, setRest: 60, endRest: 180 },
    { name: "plank", reps: 15, sets: 4, setRest: 60, endRest: 180 }
  ]
}

users = [
  "Yamikani",
  "Madalitso"
]

module.exports = {
  Exercises: exercises,
  Users: users
}