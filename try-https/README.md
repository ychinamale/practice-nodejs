View the express app at http://localhost:300

Notes:
    You will need your osslKey and openSSL certificate sitting in a `<project_dir>/secrets/` directory.

    You can create these with a command like:
    openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -keyout osslKey.key -out osslCert.crt -subj "/C=<city>/ST=<province>/L=<city>/O=<organization>/CN=<full name>"

    You will get a warning; that is because your key and certificate are not verifeid.