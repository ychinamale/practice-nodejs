const https = require('https');
const express = require('express');
const cookieParser = require('cookie-parser');
const fs = require('fs');
const port = 3000;
const app = express();

app.use(cookieParser())

const credentials = {
    key: fs.readFileSync('./secrets/osslKey.key'),
    cert: fs.readFileSync('./secrets/osslCert.crt')
}

const expiryTime = 6000


const cookieSettings = {
    httpOnly: true,
    secure: true,
    maxAge: expiryTime
}

app.get('/', (req, res)=>{
    res.cookie('secret_ingredient', 'almond milk', cookieSettings);
    // res.clearCookie('secret_ingrediet') // to unset the cookie

    res.status(200).send("Hello there, <a href='/cookie'>cookies</a>?");
})

app.get('/cookie', (req, res)=>{
    if (req.cookies.secret_ingredient !== undefined){
        res.status(200).send(`There you go ${req.cookies.secret_ingredient}`);
        console.log("Creating a cookie that holds my secret ingredient.\n Can you print my secret in your browser console?", req.cookies)
    } else {
        res.status(200).send(`Sorry bruv. ${req.cookies.secret_ingredient}`);
        console.log("Well, that cookie expired bruv")
    }
    

    console.log('the date thing is ', expiryTime)
})

https.createServer(credentials, app).listen(port);