exercises = [
    {   name: "Push ups",
        calories: 100,
        difficulty: 2
    },
    {
        name: "Squats",
        calories: 200,
        difficulty: 3
    }
]


exercises = {
    push: [
        { name: "push ups", reps: 3},
        { name: "barbells", reps: 3}
    ],
    pull: {
        name: "Squats",
        calories: 200,
        difficulty: 3
    }
}

users = [
    {   
        name: "Yamikani",
        age: 30,
        sex: "male"
    },
    {
        name: "Madalitso",
        age: "26",
        sex: "male"
    }
]

module.exports = {
    Exercises: exercises,
    Users: users
}