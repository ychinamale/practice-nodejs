const express = require('express')
const app = express()
const port = 3000

// import data.js objects
var { Exercises, Users } = require('./data')

app.get('/', (req, res) => res.send(JSON.stringify(Exercises)))

app.listen(port, () => console.log("We got some exercises ", Exercises,"and something else\nUsers are", Users))
