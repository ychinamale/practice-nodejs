{-
    Author:         Yamikani Chinamale
    Description:    My first module
-}


module FirstModule
where
    arithmeticMean x y = (x + y) /2

    harmonicMean x y = 2 * x * y / (x + y)

    getFirst (x, y) = x
    getSecond (x, y) = y

    returnSumMul x y = (x+y, x*y)

    sortNumIf x y = 
        if (x < y) 
            then (x,y)
            else (y,x)

    sortNumGuard x y
        | (x < y) = (x, y)
        | (y < x) = (y, x)
        | otherwise = (x, y)

    mangle myString
        = tail myString ++ show (head myString)


    listFromTo x y =
        [0, x..y]