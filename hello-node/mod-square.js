// module that calculates the area and perimeter of a square
module.exports = {
    area: function (width) {
        return width * width;
    },

    perimeter: function (width) {
        return width * 4;
    }
}