// load http module
const http = require("http");

const hostname = "127.0.0.1";
const port = 8000;

// create HTTP server
const server = http.createServer((aRequest, aResponse) => {
    aResponse.writeHead(200, {'Content-Type': 'text/plain'});
    aResponse.end('Hello World\n');
});

server.listen(port, hostname, ()=> {
    console.log(`Server is running at http://${hostname}:${port}/`);
});