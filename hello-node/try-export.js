var express = require('express');
var square = require('./mod-square');

var app = express();


var port = '8000';

app.get('/', function(aRequest, aResponse){
    aResponse.send('The area of a 4cm square is '+ square.area(4) + 'cm squared')
});

app.listen(port, function(){
    console.log(`Express worked with an import.`);``
})