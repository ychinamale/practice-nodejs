// This is an example of callback hell
// Fix this code by following the 3 rules

var form = document.querySelector('form')
form.onsubmit = function (submitEvent) {
  var name = document.querySelector('input').value
  request({
    uri: "http://example.com/upload",
    body: name,
    method: "POST"
  }, function (err, response, body) {
    var statusMessage = document.querySelector('.status')
    if (err) return statusMessage.value = err
    statusMessage.value = body
  })
}

// First list the 3 rules
// 1. Keep code shallow
//  Give each anon function a name
//  Hoist functions/remove nesting
//  Keep shallow using calls

// 2. Modularize

// 3. Handle every single error