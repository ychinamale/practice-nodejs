// Hello World for ExpressJS

var express = require('express');
var app = express();

var port = '3000';
var hostname = '127.0.0.1';


app.get('/', function(aRequest, aResponse) {
    aResponse.send("Hello there Express!");
});

app.listen(port, hostname, function(){
    console.log(`Express listening on ${hostname}:${port}/`);
});

/*
WORKFLOW NOTES:

// import the express module

// create an express object (app)

// create a route on that object as
// a get function that takes two params
// i.  the route
// ii. a function with req and res objects as params
    call the send function on response object to print something

// make the object listen on port by
// calling the listen function on the object
// and 


*/