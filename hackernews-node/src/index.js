const { GraphQLServer } = require('graphql-yoga');


let links = [{
    id: 'link-0',
    description: 'Learning GraphSQL',
    url: 'www.howtographsql.com'
}];

let idCount = links.length;


const resolvers = {
    Query : {
        link : (parent, args) => links.filter( link => link.id == args.id )[0]
    },
    Mutation : {
        post: (parent, args) => {
            const link = {
                id: `link-${idCount++}`,
                description: args.description,
                url: args.url,
            }

            links.push(link)

            return link
        },

        updateLink: (parent, args) => {
            const index = links.findIndex(item => item.id == args.id )
            links[index].description = args.description
            links[index].url = args.url
            return links[index]
        },

        deleteLink: (parent, args) => {
            const index = links.findIndex(item => item.id == args.id )
            const link = links[index]
            links = links.splice(index, 1)
            return link
        },
    }
};

const server = new GraphQLServer({
    typeDefs: './src/challenge01.graphql',
    resolvers,
});

const options = {
    port: 3000
}

server.start(options, ({ port })=> console.log(`Server is running on http://localhost:${port}`));