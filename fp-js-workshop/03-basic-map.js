function doubleAll(numbers) {
    // SOLUTION GOES HERE
    return numbers.map( thisNumber => thisNumber * 2 )
}

module.exports = doubleAll