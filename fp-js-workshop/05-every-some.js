function checkUsersValid(goodUsers) {
    return function allUsersValid(submittedUsers) {
        // SOLUTION GOES HERE
        return submittedUsers.every(thisUser => goodUsers.includes(thisUser) )
    };
}

module.exports = checkUsersValid