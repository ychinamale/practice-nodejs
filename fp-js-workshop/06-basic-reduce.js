function countWords(inputWords) {
    // SOLUTION GOES HERE
    let newObject = inputWords.reduce((countMap, thisWord)=>{
        countMap[thisWord] = ++countMap[thisWord] || 1
        return countMap
    },{})

    return newObject
}

module.exports = countWords