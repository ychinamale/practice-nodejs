function getShortMessages(messages) {
    // SOLUTION GOES HERE
    return messages
        .filter( thisMessage => thisMessage.message.length < 50 )
        .map( thisMessage => thisMessage.message)
}

module.exports = getShortMessages