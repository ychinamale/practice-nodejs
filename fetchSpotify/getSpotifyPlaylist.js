const secrets = require('./secrets')
const fetch = require('node-fetch')

const tokenUrl = 'https://accounts.spotify.com/api/token';

const playlistID = "5rJKdh7RyjaPnnwTKUcboF"
const spotifyPlaylistUrl = `https://api.spotify.com/v1/playlists/${playlistID}/tracks`
const queryParams = encodeURIComponent('items(added_by(uri)),items(track(name)),items(track(uri)),items(track(artists(name, uri))),items(track(album(name, uri)))');
const playlistUrlQuery = spotifyPlaylistUrl + '?fields=' + queryParams + '&market=ZA';


async function getSpotifyToken(){
    const tokenResponse = await fetch(tokenUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + secrets.clientData
        },
        body: 'grant_type=client_credentials'
    });
    return await tokenResponse.json();
}


console.log(playlistUrlQuery);

async function getSpotifyPlaylist(){
    return await getSpotifyToken().then( async (tokenData) => {
        const response = await fetch(playlistUrlQuery, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Bearer ' + tokenData.access_token
            }
        });
        return await response.json();
    });
}

getSpotifyPlaylist()
    .then( data => console.log("We got a playlist", data.items))
    .catch( error =>  console.log("We got an error, dammit!!\n",error) )


/* references:
https://developer.spotify.com/documentation/general/guides/authorization-guide/#client-credentials-flow
https://community.box.com/t5/Platform-and-Development-Forum/always-getting-Invalid-grant-type-parameter-or-parameter-missing/td-p/40732
https://github.com/spotify/web-api-auth-examples/blob/master/client_credentials/app.js
// */